#include "ArrayQueue.h"

ArrayQueue::ArrayQueue(int capacity)
{
    m_queue = new int[capacity]{};
    m_front = -1;
    m_back = -1;
    m_capacity = capacity;
    m_size = 0;
}

ArrayQueue::~ArrayQueue()
{
    delete[] m_queue;
}

void ArrayQueue::push(int data)
{
    if (m_size == m_capacity)
    {
        resize();
    }
    if (m_back == -1)
    {
        m_front = 0;
        m_back = 0;
    }
    else
    {
        m_back = (m_back + 1) % m_capacity;
    }
    m_queue[m_back] = data;
    m_size++;
}

void ArrayQueue::pop()
{
    if (m_size == 0)
    {
        return;
    }
    if (m_front == m_back)
    {
        m_front = -1;
        m_back = -1;
    }
    else
    {
        m_front = (m_front + 1) % m_capacity;
    }
    m_size--;
}

int &ArrayQueue::front()
{
    return m_queue[m_front];
}

int &ArrayQueue::back()
{
    return m_queue[m_back];
}

int ArrayQueue::size()
{
    return m_size;
}

void ArrayQueue::resize()
{
    int newCapacity = m_capacity * 2;
    int *newQueue = new int[newCapacity];

    for (int i = 0; i < m_size; i++)
    {
        newQueue[i] = m_queue[(m_front + i) % m_size];
    }
    delete[] m_queue;
    m_queue = newQueue;
    m_capacity = newCapacity;
    m_front = 0;
    m_back = m_size - 1;
}
