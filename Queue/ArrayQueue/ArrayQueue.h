#ifndef ARRAYQUEUE_H
#define ARRAYQUEUE_H

class ArrayQueue
{
private:
  int m_capacity;
  int m_size;
  int m_front;
  int m_back;
  int *m_queue;

  void resize();

public:
  ArrayQueue(int capacity);
  ~ArrayQueue();

  void push(int data);
  void pop();
  int &back();
  int &front();
  int size();
};

#endif // ARRAYQUEUE_H
