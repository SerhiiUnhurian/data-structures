#ifndef LISTQUEUE_H
#define LISTQUEUE_H

#include "Node.h"

class ListQueue
{
private:
  Node *m_front;
  Node *m_back;
  int m_size;

public:
  ListQueue();
  ~ListQueue();

  void push(int data);
  void pop();
  int &back();
  int &front();
  int size();
};

#endif // LISTQUEUE_H
