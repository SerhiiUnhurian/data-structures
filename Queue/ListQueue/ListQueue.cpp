#include "ListQueue.h"

ListQueue::ListQueue()
{
    m_front = nullptr;
    m_back = nullptr;
    m_size = 0;
}

ListQueue::~ListQueue()
{
    Node *next = nullptr;

    while (m_front != nullptr)
    {
        next = m_front->next;
        delete m_front;
        m_front = next;
    }
}

void ListQueue::push(int data)
{
    Node *node = new Node(data);

    if (m_back == nullptr)
    {
        m_front = node;
        m_back = node;
    }
    else
    {
        m_back->next = node;
        m_back = node;
    }
    m_size++;
}

void ListQueue::pop()
{
    if (m_front == nullptr)
    {
        return;
    }

    Node *tmp = m_front;

    if (m_front == m_back)
    {
        m_front = nullptr;
        m_back = nullptr;
    }
    else
    {
        m_front = m_front->next;
    }
    delete tmp;
    m_size--;
}

int &ListQueue::back()
{
    return m_back->data;
}

int &ListQueue::front()
{
    return m_front->data;
}

int ListQueue::size()
{
    return m_size;
}
