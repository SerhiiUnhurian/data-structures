#ifndef BINARY_MIN_HEAP_H
#define BINARY_MIN_HEAP_H

class BinaryMinHeap
{
private:
  int *m_heap;
  int m_size;
  int m_capacity;

  int getParent(int index);
  int getLeftChild(int index);
  int getRightChild(int index);

  bool hasLeft(int index);
  bool hasRight(int index);

  void heapifyUp(int index);
  void heapifyDown(int index);
  void swap(int index1, int index2);

public:
  BinaryMinHeap(int capacity);
  ~BinaryMinHeap();

  int size();
  bool empty();
  void push(int data);
  void pop();
  int const &top() const;

  void display();
};

#endif // BINARY_MIN_HEAP_H
