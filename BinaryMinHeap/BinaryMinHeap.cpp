#include <iostream>
#include "BinaryMinHeap.h"

BinaryMinHeap::BinaryMinHeap(int capacity)
{
    m_heap = new int[capacity]{};
    m_capacity = capacity;
    m_size = 0;
}

BinaryMinHeap::~BinaryMinHeap()
{
    delete[] m_heap;
}

int BinaryMinHeap::size()
{
    return m_size;
}

bool BinaryMinHeap::empty()
{
    return !(m_size > 0);
}

void BinaryMinHeap::push(int data)
{
    if (m_size == m_capacity)
    {
        return;
    }
    else
    {
        m_heap[m_size] = data;
        m_size++;
        heapifyUp(m_size - 1);
    }
}

void BinaryMinHeap::pop()
{
    if (m_size == 0)
    {
        return;
    }

    m_heap[0] = m_heap[m_size - 1];
    m_size--;

    if (m_size > 1)
    {
        heapifyDown(0);
    }
}

int const &BinaryMinHeap::top() const
{
    return m_heap[0];
}

int BinaryMinHeap::getParent(int index)
{
    return (index - 1) / 2;
}

int BinaryMinHeap::getLeftChild(int index)
{
    return 2 * index + 1;
}

int BinaryMinHeap::getRightChild(int index)
{
    return 2 * index + 2;
}

bool BinaryMinHeap::hasLeft(int index)
{
    return getLeftChild(index) < m_size;
}

bool BinaryMinHeap::hasRight(int index)
{
    return getRightChild(index) < m_size;
}

void BinaryMinHeap::swap(int index1, int index2)
{
    int tmp = m_heap[index1];
    m_heap[index1] = m_heap[index2];
    m_heap[index2] = tmp;
}

void BinaryMinHeap::heapifyUp(int index)
{
    if (index != 0)
    {
        int parent = getParent(index);

        if (m_heap[parent] > m_heap[index])
        {
            swap(parent, index);
            heapifyUp(parent);
        }
    }
}

void BinaryMinHeap::heapifyDown(int index)
{
    int left = getLeftChild(index);
    int right = getRightChild(index);
    int min = index;

    if (hasLeft(index) && m_heap[left] < m_heap[min])
    {
        min = left;
    }
    if (hasRight(index) && m_heap[right] < m_heap[min])
    {
        min = right;
    }
    if (min != index)
    {
        swap(index, min);
        heapifyDown(min);
    }
}

void BinaryMinHeap::display()
{
    for (int i = 0; i < m_size; i++)
    {
        std::cout << m_heap[i] << ' ';
    }
    std::cout << '\n';
}
