#include <iostream>
#include "BinaryMinHeap.h"

int main()
{
    BinaryMinHeap heap(7);
    heap.push(15);
    heap.push(1);
    heap.push(13);
    heap.push(10);
    heap.push(5);
    heap.push(7);
    heap.push(9);
    heap.display();

    while (!heap.empty())
    {
        std::cout << heap.top() << ' ';
        heap.pop();
    }

    std::cout << '\n'
              << heap.size() << std::endl;

    return 0;
}
