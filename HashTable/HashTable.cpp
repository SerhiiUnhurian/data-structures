#include "HashTable.h"

HashTable::HashTable() {}

int HashTable::hashFunction(int key)
{
    return key % TABLE_SIZE;
}

bool HashTable::isEmpty()
{
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        if (!tableList[i].empty())
        {
            return false;
        }
    }
    return true;
}

void HashTable::insert(int key, std::string value)
{
    bool isKeyFound = false;
    int hashKey = hashFunction(key);

    for (auto &keyValue : tableList[hashKey])
    {
        if (keyValue.first == key)
        {
            keyValue.second = value;
            isKeyFound = true;
        }
    }

    if (!isKeyFound)
    {
        tableList[hashKey].emplace_back(key, value);
    }
}

void HashTable::remove(int key)
{
    int hashKey = hashFunction(key);
    auto &cell = tableList[hashKey];

    for (auto it = cell.begin(); it != cell.end(); it++)
    {
        if (it->first == key)
        {
            cell.erase(it);
            return;
        }
    }
}

std::string HashTable::search(int key)
{
    int hashKey = hashFunction(key);

    for (auto &keyValue : tableList[hashKey])
    {
        if (keyValue.first == key)
        {
            return keyValue.second;
        }
    }
    return "";
}
