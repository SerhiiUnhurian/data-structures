#include <iostream>
#include "HashTable.h"

int main()
{
    HashTable *hashTable = new HashTable();

    bool b = hashTable->isEmpty();
    if (b)
        std::cout << "Hash table is empty";
    else
        std::cout << "Hash table is not empty";
    std::cout << std::endl;

    std::cout << "Adding a data" << std::endl;
    hashTable->insert(434, "Dylan");

    b = hashTable->isEmpty();
    if (b)
        std::cout << "Hash table is empty";
    else
        std::cout << "Hash table is not empty";
    std::cout << std::endl;

    hashTable->insert(391, "Dominic");
    hashTable->insert(806, "Adam");
    hashTable->insert(117, "Lindsey");
    hashTable->insert(548, "Cameron");
    hashTable->insert(669, "Terry");
    hashTable->insert(722, "Brynn");
    hashTable->insert(276, "Jody");
    hashTable->insert(953, "Frankie");
    hashTable->insert(895, "Vanessa");

    int key = 669;
    std::cout << "Search value for key ";
    std::cout << key << std::endl;

    std::string name = hashTable->search(key);
    if (name != "") 
    {
        std::cout << "Value for key " << key;
        std::cout << " is " << name;
    }
    else 
    {
        std::cout << "Value for key	" << key;
        std::cout << " is not found";
    }
    std::cout << std::endl;

    std::cout << "Remove node of key ";
    std::cout << key << std::endl;
    hashTable->remove(key);

    name = hashTable->search(key);
    if (name != "") 
    {
        std::cout << "Value for key " << key;
        std::cout << " is " << name;
    }
    else 
    {
        std::cout << "Value for key " << key;
        std::cout << " is not found";
    }
    std::cout << std::endl;

    return 0;
}
