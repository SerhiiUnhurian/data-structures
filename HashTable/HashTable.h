#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <list>
#include <string>

#define TABLE_SIZE 7

class HashTable
{
private:
  std::list<std::pair<int, std::string>> tableList[TABLE_SIZE];

  int hashFunction(int key);

public:
  HashTable();

  bool isEmpty();
  void insert(int key, std::string value);
  void remove(int key);
  std::string search(int key);
};

#endif // HASH_TABLE_H
