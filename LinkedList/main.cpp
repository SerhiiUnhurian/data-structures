#include <iostream>
#include "LinkedList.h"

int main()
{
    LinkedList list;
    list.push_back(1);
    list.push_back(2);
    list.insert(2, 23);
    list.push_back(3);
    list.push_front(4);
    list.push_front(5);
    list.toString();
    list.erase(6);

    // list.erase(3);
    // list.erase(1);
    // list.erase(1);
    // list.erase(1);
    // list.erase(1);
    // list.erase(1);
    // list.pop_front();
    // list.pop_back();
    // list.pop_back();
    // list.pop_back();
    // list.pop_back();
    // list.pop_back();
    list.toString();
    cout << list.getSize();

    return 0;
}
