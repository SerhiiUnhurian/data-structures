#include <iostream>
#include "LinkedList.h"
#include "Node.h"

using namespace std;

LinkedList::LinkedList()
{
    head = nullptr;
    tail = nullptr;
    size = 0;
}

LinkedList::~LinkedList()
{
    Node *next = nullptr;

    while (head != nullptr)
    {
        next = head->next;
        delete head;
        head = next;
    }
}

int LinkedList::getSize()
{
    return size;
}

void LinkedList::push_back(int data)
{
    Node *node = new Node(data);

    if (head == nullptr)
    {
        head = node;
        tail = node;
    }
    else
    {
        tail->next = node;
        tail = node;
    }
    size++;
}

void LinkedList::push_front(int data)
{
    Node *node = new Node(data);

    if (head == nullptr)
    {
        head = node;
        tail = node;
    }
    else
    {
        node->next = head;
        head = node;
    }
    size++;
}

void LinkedList::pop_front()
{
    if (head != nullptr)
    {
        Node *tmp = head;
        head = head->next;

        delete tmp;
        size--;

        if (size == 0)
        {
            tail = nullptr;
        }
    }
}

void LinkedList::pop_back()
{
    if (head == nullptr)
    {
        return;
    }
    else if (head->next == nullptr)
    {
        // pop_front();
        delete head;
        head = nullptr;
        tail = nullptr;
        size--;
    }
    else
    {
        Node *prev = nullptr;
        Node *current = head;

        while (current->next != nullptr)
        {
            prev = current;
            current = current->next;
        }
        prev->next = nullptr;
        tail = prev;
        delete current;
        size--;
    }
}

void LinkedList::insert(int pos, int data)
{
    if (pos < 0 || pos > size)
    {
        return;
    }
    else if (pos == 0)
    {
        push_front(data);
    }
    else if (pos == size)
    {
        push_back(data);
    }
    else if (head != nullptr)
    {
        Node *node = new Node(data);
        Node *prev = nullptr;
        Node *current = head;

        for (int i = 0; i < pos; i++)
        {
            prev = current;
            current = current->next;
        }
        prev->next = node;
        node->next = current;
        size++;
    }
}

void LinkedList::erase(int pos)
{
    if (pos < 0 || pos >= size)
    {
        return;
    }
    else if (pos == 0)
    {
        pop_front();
    }
    else if (pos == size - 1)
    {
        pop_back();
    }
    else if (head != nullptr)
    {
        Node *prev = nullptr;
        Node *current = head;

        for (int i = 0; i < pos; i++)
        {
            prev = current;
            current = current->next;
        }
        prev->next = current->next;
        delete current;
        size--;
    }
}

void LinkedList::toString()
{
    Node *tmp = head;

    while (tmp != nullptr)
    {
        cout << tmp->data << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}
