#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "Node.h"

using namespace std;

class LinkedList
{
private:
  Node *head;
  Node *tail;
  int size;

public:
  LinkedList();
  ~LinkedList();

  int getSize();
  void push_back(int data);
  void push_front(int data);
  void pop_front();
  void pop_back();
  void insert(int pos, int data);
  void erase(int pos);
  void toString();
};

#endif // LINKEDLIST_H
