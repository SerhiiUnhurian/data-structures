#include <iostream>
#include "BST.h"

int main()
{
    BST* btree = new BST();
    
    btree->add(5);
    btree->add(2);
    btree->add(8);
    btree->add(6);
    btree->add(1);
    btree->remove(5);

    delete btree;

    return 0;    
}