#ifndef BST_H
#define BST_H

#include "Node.h"

class BST
{
private:
  Node *m_root;

  void insert(int data, Node *&root);
  void erase(int data, Node *&root);
  Node *findMin(Node *root);

public:
  BST();
  ~BST();

  void add(int data);
  void remove(int data);
};

#endif // BST_H