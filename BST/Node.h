#ifndef NODE_H
#define NODE_H

class Node
{
public:
  int m_data;
  Node *m_left;
  Node *m_right;

  Node(int data);
};

#endif // NODE_H
