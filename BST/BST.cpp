#include <iostream>

#include "Node.h"
#include "BST.h"

BST::BST()
{
  m_root = nullptr;
}

BST::~BST()
{
    while(m_root) {
        std::cout << "deleting " << m_root->m_data << std::endl;
        erase(m_root->m_data, m_root);
    }
}

void BST::add(int data)
{
  insert(data, m_root);
}

void BST::remove(int data)
{
  erase(data, m_root);
}

void BST::insert(int data, Node *&root)
{
  if (root == nullptr)
  {
    root = new Node(data);
  }
  else if (data < root->m_data)
  {
    insert(data, root->m_left);
  }
  else if (data > root->m_data)
  {
    insert(data, root->m_right);
  }
}

void BST::erase(int data, Node *&root)
{
  if (root == nullptr)
  {
    return;
  }
  else if (data < root->m_data)
  {
    erase(data, root->m_left);
  }
  else if (data > root->m_data)
  {
    erase(data, root->m_right);
  }
  else // when data == root->m_data
  {
    if (root->m_left == nullptr && root->m_right == nullptr)
    {
      delete root;
      root = nullptr;
    }
    else if (root->m_left == nullptr)
    {
      Node *tmp = root;
      root = root->m_right;
      delete tmp;
    }
    else if (root->m_right == nullptr)
    {
      Node *tmp = root;
      root = root->m_left;
      delete tmp;
    }
    else // when root has two children
    {
      // search min data in the right node
      Node *tmp = findMin(root->m_right); 
      root->m_data = tmp->m_data;
      erase(tmp->m_data, root->m_right);
    }
  }
}

Node* BST::findMin(Node *root)
{
  while (root->m_left != nullptr)
  {
    root = root->m_left;
  }
  return root;
}
