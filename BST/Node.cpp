#include "Node.h"

Node::Node(int data)
{
    m_data = data;
    m_left = nullptr;
    m_right = nullptr;
}
