#include <iostream>
#include "ListQueue.h"

using namespace std;

int main()
{
    ListQueue<int> queue;
    queue.push(1);
    queue.push(2);
    queue.push(3);
    queue.pop();
    queue.push(4);
    cout << queue.front() << endl;
    cout << queue.back() << endl;
    cout << queue.size() << endl;
}
