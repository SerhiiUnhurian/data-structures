#include "ListQueue.h"

template <class T>
ListQueue<T>::ListQueue()
{
    m_front = nullptr;
    m_back = nullptr;
    m_size = 0;
}

template <class T>
ListQueue<T>::~ListQueue()
{
    Node<T> *next = nullptr;

    while (m_front != nullptr)
    {
        next = m_front->next;
        delete m_front;
        m_front = next;
    }
}

template <class T>
void ListQueue<T>::push(T data)
{
    Node<T> *node = new Node<T>(data);

    if (m_back == nullptr)
    {
        m_front = node;
        m_back = node;
    }
    else
    {
        m_back->next = node;
        m_back = node;
    }
    m_size++;
}

template <class T>
void ListQueue<T>::pop()
{
    if (m_front == nullptr)
    {
        return;
    }

    Node<T> *tmp = m_front;

    if (m_front == m_back)
    {
        m_front = nullptr;
        m_back = nullptr;
    }
    else
    {
        m_front = m_front->next;
    }
    delete tmp;
    m_size--;
}

template <class T>
T &ListQueue<T>::back()
{
    return m_back->data;
}

template <class T>
T &ListQueue<T>::front()
{
    return m_front->data;
}

template <class T>
int ListQueue<T>::size()
{
    return m_size;
}


template class ListQueue<int>;
