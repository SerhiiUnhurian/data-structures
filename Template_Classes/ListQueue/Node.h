#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

template <class T>
class Node
{
public:
  T data;
  Node<T> *next;

  Node(T data);
};

#endif // NODE_H
