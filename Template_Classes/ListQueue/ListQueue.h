#ifndef LISTQUEUE_H
#define LISTQUEUE_H

#include "Node.h"

template <class T>
class ListQueue
{
private:
  Node<T> *m_front;
  Node<T> *m_back;
  int m_size;

public:
  ListQueue();
  ~ListQueue();

  void push(T data);
  void pop();
  T &back();
  T &front();
  int size();
};

#endif // LISTQUEUE_H
