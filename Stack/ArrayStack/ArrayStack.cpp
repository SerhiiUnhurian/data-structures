#include "ArrayStack.h"

ArrayStack::ArrayStack(int capacity)
{
    this->capacity = capacity;
    this->stack = new int[capacity]{};
    this->top = -1;
}
ArrayStack::~ArrayStack()
{
    delete[] stack;
}

void ArrayStack::push(int data)
{
    if (top == capacity - 1)
    {
        resize();
    }
    stack[++top] = data;
}

void ArrayStack::pop()
{
    if (top > -1)
    {
        top--;
    }
    // delete stack[top--];
}

int &ArrayStack::getTop()
{
    return stack[top];
}

int ArrayStack::size()
{
    return top + 1;
}

void ArrayStack::resize()
{
    int newCapacity = capacity * 2;
    int *newStack = new int[newCapacity]{};

    for (int i = 0; i < capacity; i++)
    {
        newStack[i] = stack[i];
    }

    delete[] stack;
    stack = newStack;
    capacity = newCapacity;
}
