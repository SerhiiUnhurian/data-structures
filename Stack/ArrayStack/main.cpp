#include "ArrayStack.h"
#include <iostream>

using namespace std;

int main()
{
    ArrayStack stack(3);

    stack.push(1);
    stack.push(2);
    stack.push(3);

    cout << stack.size() << endl;
    stack.pop();
    stack.pop();

    cout << stack.getTop() << endl;
    // cout << stack.size();

    return 0;
}
