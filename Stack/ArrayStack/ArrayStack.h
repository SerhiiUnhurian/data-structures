#ifndef ARRAYSTACK_H
#define ARRAYSTACK_H

class ArrayStack
{
private:
  int capacity;
  int top;
  int *stack;

  void resize();

public:
  ArrayStack(int capacity);
  ~ArrayStack();

  void push(int data);
  void pop();
  int &getTop();
  int size();
};

#endif // ARRAYSTACK_H
