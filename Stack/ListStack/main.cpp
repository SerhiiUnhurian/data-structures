#include "ListStack.h"

using namespace std;

int main()
{
    ListStack stack;
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.pop();

    cout << stack.top() << endl;
    cout << stack.size() << endl;

    return 0;
}
