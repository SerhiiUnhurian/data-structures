#ifndef LISTSTACK_H
#define LISTSTACK_H

#include "Node.h"

class ListStack
{
private:
  Node *m_top;
  int m_size;

public:
  ListStack();
  ~ListStack();

  void push(int data);
  void pop();
  int &top();
  int size();
};

#endif // LISTSTACK_H
