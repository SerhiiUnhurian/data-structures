#include "ListStack.h"
#include "Node.h"

ListStack::ListStack()
{
    m_top = nullptr;
    m_size = 0;
}

ListStack::~ListStack()
{
    Node *next = nullptr;

    while (m_top != nullptr)
    {
        next = m_top->next;
        delete m_top;
        m_top = next;
    }
}

void ListStack::push(int data)
{
    Node *node = new Node(data);
    node->next = m_top;
    m_top = node;
    m_size++;
}

void ListStack::pop()
{
    if (m_top != nullptr)
    {
        Node *tmp = m_top;
        m_top = m_top->next;
        delete tmp;
        m_size--;
    }
}

int &ListStack::top()
{
    return m_top->data;
}

int ListStack::size()
{
    return m_size;
}
